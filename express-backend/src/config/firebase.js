const DATABASE_URL = "https://booking-app-e7c0f.firebaseio.com";

const admin = require('firebase-admin');

/* 
  Please set path to where your firebase keys resides. This will not work for you since
  serviceAccountKey.json file resides locally in the machine. 
*/
const serviceAccount = require('../../../firebase_crendentials/serviceAccountKey.json');

// Get the FieldValue object
const FieldValue = require('firebase-admin').firestore.FieldValue;
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: DATABASE_URL,
});

const db = admin.firestore();

module.exports = {db, FieldValue};