const { db, FieldValue} = require('../config/firebase');
const eventCollection = db.collection('events');
const doctorCollection = db.collection('doctors');

/**
 * @typedef {Object} Event
 * @property {string} id - The event id
 * @property {string} inviteeId
 * @property {Date} startTime
 * @property {Date} endTime
 */


/**
 * @param {string} doctorId
 */
function fetchDoctorInfo(doctorId) {
    return doctorCollection.doc(doctorId).get();
}

/**
 * @param {string} doctorId
 * @param {Date} startTime
 * @param {Date} endTime
 * @returns {Promise<Event[]>}
 */
async function fetchEvents(doctorId, startTime, endTime) {

    async function eventsPromise(resolve, reject) {
        const snapshot = await eventCollection
            .where('doctorId', '==', doctorId)
            .where('startTime', '>=', startTime)
            .where('startTime', '<=', endTime)
            .get();

        if(snapshot.empty) {
            resolve([]);
        } else {
            resolve(snapshot.docs.map((documentData) => {
                return {
                    id: documentData.id,
                    ...documentData.data(),
                };
            }));
        }
    }

    return new Promise(eventsPromise);   
}

/**
 * @param {string} doctorId
 * @param {string} inviteeId
 * @param {Date} startTime
 * @param {Date} endTime
 */
async function createEvent(
    doctorId,
    inviteeId,
    startTime,
    endTime,
) {
    
    return eventCollection.add({
        createdAt: FieldValue.serverTimestamp(),
        startTime: startTime,
        endTime: endTime,
        doctorId,
        inviteeId,
    });
}

module.exports = {
    fetchDoctorInfo,
    fetchEvents,
    createEvent
}