const {
    fetchDoctorInfo,
    fetchEvents,
} = require("../database/database");

const Moment = require('moment-timezone')
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);

/**
 * @typedef {string} Event
 * @property {string} id - The event id
 * @property {string} inviteeId
 * @property {Date} startTime
 * @property {Date} endTime
 */


/**
 * @param {string} doctorId
 * @param {Date} date
 * @param {string} userTimezone
 * @param {string} slotDuration
 * @returns {Promise<Date[]>}
 */
async function getAvailableTimes(
    doctorId,
    date,
    userTimezone,
    slotDuration,
) {

    const docRef = await fetchDoctorInfo(doctorId)
    
    const { startTime, endTime, timezone: doctorTimezone } = docRef.data().businessHours;

    const beginDayTimezone = moment.tz(new Date(date), userTimezone).startOf('day')
    const endDayTimezone = moment.tz(new Date(date), userTimezone).endOf('day')

    const events = await fetchEvents(doctorId, beginDayTimezone, endDayTimezone);

    // this is the date/time from when user wants to see all available slots

    let freeSlots = getFreeSlotsIntern(
        date,
        events,
        userTimezone,
        slotDuration,
        startTime,
        endTime,
        doctorTimezone,
    );

    freeSlots = freeSlots.map(date => {
        return date.tz(userTimezone).format("YYYY-MM-DD HH:mm:ss").toString();
    });
    
    return freeSlots;
}

/**
 * @param {Date} userDate
 * @param {Array<Event>} events
 * @param {string} userTimezone
 * @param {number} slotDuration
 * @param {Date} businessStartTime
 * @param {Date} businessEndTime
 * @param {string} doctorTimezone
 * @returns {Promise<Event[]>}
 */
function getFreeSlotsIntern(
    userDate,
    events,
    userTimezone,
    slotDuration,
    businessStartTime,
    businessEndTime,
    doctorTimezone,
) {
    let timeSlots = [];

    if (userTimezone === doctorTimezone) {
        let startInterval = moment(new Date(userDate))
            .tz(userTimezone)
            .startOf('day')
            .hours(businessStartTime);

        let endInterval = moment(new Date(userDate))
            .tz(userTimezone)
            .startOf('day')
            .hours(businessEndTime);

        timeSlots = generateFreeSlotsBetweenIntervals(
            events,
            startInterval,
            endInterval,
            slotDuration
        );
    } else {
        const userDayBegins = moment(new Date(userDate))
            .tz(userTimezone)
            .startOf('day');

        const userDayEnds = moment(new Date(userDate))
            .tz(userTimezone)
            .endOf('day');

        const businessPreviousDayBegins = userDayBegins.clone()
            .tz(doctorTimezone)
            .startOf('day')
            .hours(businessStartTime);

        const businessPreviousDayEnds = userDayBegins.clone()
            .tz(doctorTimezone)
            .startOf('day')
            .hours(businessEndTime);

        let startIntervalOne;
        let endIntervalOne;

        if (businessPreviousDayBegins.isAfter(userDayBegins, 'minute')) {
            startIntervalOne = businessPreviousDayBegins;
        } else {
            startIntervalOne = userDayBegins;
        }

        if (businessPreviousDayEnds.isAfter(userDayEnds, 'minute')) {
            endIntervalOne = userDayEnds;
        } else {
            endIntervalOne = businessPreviousDayEnds;
        }

        timeSlots = generateFreeSlotsBetweenIntervals(
            events,
            startIntervalOne,
            endIntervalOne,
            slotDuration,
            userTimezone,
        )

        let startIntervalTwo;
        let endIntervalTwo;

        const businessCurrentDayBegins = userDayEnds
            .clone()
            .tz(doctorTimezone)
            .startOf('day')
            .hours(businessStartTime);

        if (businessCurrentDayBegins.isAfter(userDayEnds, 'minute')) {
            // do nothing
        } else {
            startIntervalTwo = businessCurrentDayBegins;
            endIntervalTwo = userDayEnds;

            timeSlots = timeSlots.concat(generateFreeSlotsBetweenIntervals(
                events,
                startIntervalTwo,
                endIntervalTwo,
                slotDuration,
                userTimezone
            ));
        }
    }

    return timeSlots;    
}


/**
 * @param {Array<Event>} events
 * @param {Date} startInterval
 * @param {Date} endInterval
 * @param {number} slotDuration
 * @returns {moment.Moment[]}
 */
function generateFreeSlotsBetweenIntervals(
    events,
    startInterval,
    endInterval,
    slotDuration,
    userTimezone,
) {
    let intervalRange = moment.range(startInterval,endInterval);
    let timeSlots = [];

    if (events.length === 0) {
        timeSlots = Array.from(intervalRange.by('minutes', {
            step: slotDuration,
            excludeEnd: true
        }));
    } else {

        const endIntervalMoment = moment(endInterval);

        let currentSlot = moment(startInterval);

        while (currentSlot.isBefore(endIntervalMoment)) {
            let nextSlot = currentSlot.clone().add(slotDuration, 'minutes');
            if (events.length > 0) {

                if (nextSlot.isBetween(
                    moment(events[0].startTime.toDate()),
                    moment(events[0].endTime.toDate())
                )) {
                    // skip
                    currentSlot = moment(events[0].endTime.toDate());
                    events.shift();
                    
                } else if (currentSlot.isSame(moment(events[0].startTime.toDate()))) {
                    currentSlot = moment(events[0].endTime.toDate());
                    events.shift();
                } else {
                    timeSlots.push(currentSlot);
                    currentSlot = nextSlot;
                }
            } else {
                timeSlots.push(currentSlot);
                currentSlot = nextSlot;
            }
        }
    }

    return timeSlots;
}

module.exports = {
    getAvailableTimes
}