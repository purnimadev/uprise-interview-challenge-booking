const moment = require('moment');
const { createEvent } = require('../database/database');

/**
 * @param {string} doctorId
 * @param {string} inviteeId
 * @param {Date} date
 * @param {string} slotDuration
 * @returns {Promise<Date[]>}
 */
async function bookEvent(
    doctorId,
    inviteeId,
    date,
    slotDuration,
) {
    const { startTime, endTime } = formatData(date, slotDuration);
    
    const event = await createEvent(
        doctorId,
        inviteeId,
        startTime,
        endTime,
    );

    return event;
}

function formatData(date, slotduration) {
    const startTime = moment(date);
    const endTime = moment(startTime).add(slotduration, 'minutes');

    return { startTime, endTime };
}

module.exports = {
    bookEvent,
};