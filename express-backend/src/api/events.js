const express = require('express');
const router = express.Router();

const { bookEvent } = require('../services/events'); 

async function createEventHandler(req, res, next) {
  try {
    if(req.body) {
      const { date, slotduration } = req.body;
      
      const doctorId = "YdQKdOxB0L2nAi2WvHim";
      const inviteeId = "pumpkin@example.com";

      const event = await bookEvent(
        doctorId,
        inviteeId,
        date,
        slotduration,
      );
      
      if(event) {
        res.status(200).json({ success:  true})
      }

    }

  } catch (err) {
    console.log(err);
    res.status(500).json({ "error": true});
  }
}

router.post('/', createEventHandler);

module.exports = router;