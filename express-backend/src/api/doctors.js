const express = require('express');
const router = express.Router();
const { getAvailableTimes } = require('../services/doctors')


async function getAvailableTimesHandler(req, res) {
    try {
        const { id } = req.params;
        const { timezone, slotduration, date} = req.query;
        const allSlots = await getAvailableTimes(id, date, timezone, slotduration);
        res.status(200).json({ "success": true, "availableSlots": allSlots });

    } catch(err) {
        console.log(err)
        res.status(500).json({ "error": true, "availableSlots": [] });
    }
}


router.get('/:id/slots', getAvailableTimesHandler);

module.exports = router;