const express = require('express');
const init = require('./src/loaders/index');
const events = require('./src/api/events');
const doctors = require('./src/api/doctors');
const cors = require('cors');

const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

let app = express();

const APP_URL = "http://localhost:3000";
const API_VERSION = 'v1';

app.use(cors({
  origin: APP_URL,
}));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev')); 
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));


app.use(`/api/${API_VERSION}/events`, events);
app.use(`/api/${API_VERSION}/doctors`, doctors);

init({app});
  
module.exports = app;
