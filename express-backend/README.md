# Booking-events App Backend code

## Instructions to install:

- Clone this repo
- `cd` to this directory, and then run `npm install`
- Run `npm start`

## Project dependencies

- NodeJS v14+
- Firebase cloudstore database


Note: You will require Firebase API keys (stored in JSON format), please provide your own, or alternatively send an email to `purnimaguptapccs@gmail.com` to request for one.
