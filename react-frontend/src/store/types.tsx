import { BookingScreenStateProps } from "../screens/Booking/store/types";


export interface StateProps {
  bookingScreen: BookingScreenStateProps;
 
}