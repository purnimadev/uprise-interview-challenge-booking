import React from 'react';
import { connect } from 'react-redux';
import {
  Redirect,
  Route,
  Switch,
  RouteComponentProps,
  RouteProps,
} from "react-router-dom";
import styled from 'styled-components';
// @ts-ignore
import { backgrounds } from '@uprise/colors';
import './App.css';

import Booking from './screens/Booking';

function App() {
  return (
    <StyledApp className="App">
      <Switch>                     
        <Route path="/booking" component={Booking}/>
        <Redirect from="/" to = "/booking" exact />
      </Switch>
    </StyledApp>
  );
}

const StyledApp = styled.div `
  background-color: ${backgrounds.fadedPurple};
  height: 100vh; 
`
export default App;
