import React from 'react';
// @ts-ignore
import { Container, Row, Col } from '@uprise/grid';
// @ts-ignore
import Header from '../../../core/components/Header';
// @ts-ignore
import Card from '../../../core/components/Card';
// @ts-ignore
import success from '@uprise/icons/icons/png/success/success.png';
// @ts-ignore
import { H2 } from '@uprise/headings';


export default function BookingSuccess() {
    return (
        <>
            <Header title="Booking a Coaching" />
            
            <Container>
                <Card>               
                    <Row>
                        <Col className="col-lg-12">
                            <img src={success} alt="booking is successful"/>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="col-lg-12">
                            <H2>Booking is successful.</H2>            
                        </Col>                
                    </Row>                           
                </Card>
            </Container>
        </>
    )
}
