import React, { MouseEvent } from 'react';
import styled from 'styled-components';

import Card from '../../../core/components/Card/index';
import Header from '../../../core/components/Header/index';
import Button from '../../../core/components/Button';

// @ts-ignore
import { Container, Row, Col } from '@uprise/grid';

import queryString from 'query-string';
import { history } from '../../../config/network';
import { initializeToTimezone, formatTimeForRange } from '../utils/dates';

function getQueryToken(queryParam : any){
    const query = queryString.parse(queryParam);
    return query
}
interface Props {
    availableSlots: string[];
    location: any;
    createEvent: (date: Object | null, duration: number) => Promise<void>;
} 

export default function SlotSelect(props: Props) {
    const { createEvent, availableSlots, location } = props;
    const query = getQueryToken(location.search);
    const duration = Number(query.duration);


    const bookEvent = (event: MouseEvent<HTMLButtonElement>) => {
        const date = initializeToTimezone(event.currentTarget.value, query.timezone as string);

        if (date.isValid()) {
            createEvent(date, duration).then(() => {
                history.push(`/booking/slots/success`);
            }); 
        }
    }
    
    const freeSlots =  availableSlots.map((time, index) => {
  
        return (
            <StyledColumn className="col-lg-4" key={index}>
                <Button                   
                    title={formatTimeForRange(time, duration)} 
                    size="medium" 
                    fullWidth={false} 
                    variant="tertiary"
                    handleClick={bookEvent}
                    value={availableSlots[index]}
                />
            </StyledColumn>            
        )
    });

    return (
        <>
            <Header title="Available Slots"/>
            <Container>
                <Card>
                    <Row >                    
                        {freeSlots}         
                    </Row>
                </Card>
            </Container>
        </>    
    )
}

const StyledColumn = styled(Col)`
    margin-bottom: 30px;
`;