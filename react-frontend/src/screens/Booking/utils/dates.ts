import moment from 'moment-timezone';

export const timeZones = [
    { value: 'Australia/Sydney', label: 'Sydney' },
    { value: 'Asia/Kolkata', label: 'Kolkata' },
    { value: 'America/Los_Angeles', label: 'Los Angeles' },
];

export const callDurations = [
    { value: 15, label: '15 Minutes' },
    { value: 30, label: '30 Minutes' },
    { value: 60, label: '60 Minutes' }
];

export function initializeToTimezone(dateString: string, timezone: string) {
    console.log(dateString);
    return moment.tz(dateString, timezone);
}

export function formatTimeForRange(dateTime: string, duration: number) {
    return `${moment(dateTime).format("hh:mm a")} - ${moment(dateTime).add(duration, 'minutes').format("hh:mm a")}`
}