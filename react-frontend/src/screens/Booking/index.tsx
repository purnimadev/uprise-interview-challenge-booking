import React from 'react';
import { connect } from 'react-redux';
import {
  Redirect,
  Route,
  Switch,
  RouteComponentProps,
} from "react-router-dom";

import SlotSettings from './SlotSettings';
import SlotSelect from './SlotSelect/index';
import { getAvailableSlots } from './store/selector';
import { StateProps } from '../../store/types';
import { fetchFreeSlots, createEvent } from './store/action';
import BookingSuccess from './BookingSuccess';

interface LocalStateProps {
  availableSlots: string[];
}

export interface location extends RouteComponentProps<{location: string}> {};

interface ActionProps {
  fetchFreeSlots: (date: Object | null, timezone: string, slotDuration: number) => Promise<void>;
  createEvent: (date: Object | null, slotDuration: number) => Promise<void>;
  location: any;
}

type Props = LocalStateProps & ActionProps;

function Booking(props: Props) {
    return (
        <>
        <Switch>                  
          <Route path="/booking/slots/settings" component={() =>
            <SlotSettings fetchFreeSlots={props.fetchFreeSlots}/>
          }/>
          <Route path="/booking/slots/select" component={() => 
            <SlotSelect
              availableSlots={props.availableSlots}
              createEvent={props.createEvent}
              location={props.location}
            />
          }/>
          <Route path="/booking/slots/success" component={BookingSuccess}/>
          
          <Redirect from="/booking" to = "booking/slots/settings" exact />
        </Switch>
        </>
    );
}

function mapStateToProps(state: StateProps, { location}: RouteComponentProps): LocalStateProps {
  return {
    availableSlots: getAvailableSlots(state)
  }
}


export default connect(mapStateToProps, {
  fetchFreeSlots: fetchFreeSlots,
  createEvent: createEvent
})(Booking);