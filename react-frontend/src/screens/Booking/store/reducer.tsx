import { BookingScreenStateProps } from "./types";
import { BookingActionTypes } from '../../../config/constant';

export const bookingScreenStateProps: BookingScreenStateProps = {
    availableSlots: [],
    message: {
        success: '',
        error: ''
    }
}

export function bookingSlotReducer(
    state: BookingScreenStateProps = bookingScreenStateProps,
    action: any
): BookingScreenStateProps {
    switch (action.type) {
        case BookingActionTypes.FETCH_FREE_SLOTS_SUCCESS:
            return {
                ...state,
                availableSlots: action.payload.data.availableSlots,
                message: {
                    success: action.payload.data.success
                }
            }
        case BookingActionTypes.FETCH_FREE_SLOTS_ERROR:
            return {
                ...state,
                message: {
                    error: action.payload.data? action.payload.data.error : ""
                }
            }
        case BookingActionTypes.CREATE_EVENT_SUCCESS:
            return {
                ...state,
                message: {
                    success: action.payload.data.success
                }
            }
        default:
            return state;
    }
}


