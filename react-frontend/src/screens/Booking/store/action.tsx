import { api } from '../../../api/api';
import { history } from '../../../config/network';
import { BookingActionTypes } from '../../../config/constant';
  
export const success = (actionType: any, data: any) => {
	return {
		type: actionType,
		payload: {
			data: data
		}
	}
}
export const error = (actionType: any, data?: any) => {
	return {
		type: actionType,
		payload: {
			data: data
		},
		error: true
	}
}

export function fetchFreeSlots(date: Object | null, timezone: string, slotDuration: number) {
	return (dispatch: any) => {
		dispatch({type: BookingActionTypes.FETCH_FREE_SLOTS_PENDING})
		return api.get(date,timezone, slotDuration)
			.then((response: any) => {
				if(response.status === 200) {
					dispatch(success(BookingActionTypes.FETCH_FREE_SLOTS_SUCCESS, response.data))
					history.push(`/booking/slots/select?duration=${slotDuration}&&timezone=${timezone}`);
					return;
				}
			}).catch((err: any) => {
				dispatch(error(BookingActionTypes.FETCH_FREE_SLOTS_ERROR))
				console.log(err);
			});
	};
}

export function createEvent(datetime: Object | null, slotDuration: number,) {
	return (dispatch: any) => {
		dispatch({type: BookingActionTypes.EVENT_REQUEST_PENDING})

		return api.post(datetime, slotDuration)
			.then((response: any) => {
				if(response.status === 200) {
					dispatch(success(BookingActionTypes.CREATE_EVENT_SUCCESS, response.data))
				}
			})
			.catch(err => {
				dispatch(error(BookingActionTypes.CREATE_EVENT_ERROR))
				console.log(err);
			});
	};
}