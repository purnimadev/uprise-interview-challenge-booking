import { StateProps } from "../../../store/types";

export function getAvailableSlots( stateProps: StateProps) {
  return stateProps.bookingScreen.availableSlots;
}