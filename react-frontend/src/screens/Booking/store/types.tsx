export interface BookingScreenStateProps {
  availableSlots: Array<string>,
  message: {
    error?: string,
    success?: string
  }
};
