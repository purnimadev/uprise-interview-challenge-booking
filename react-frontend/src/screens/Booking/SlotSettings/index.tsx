import React, { useState } from 'react';
// @ts-ignore
import { Container, Row, Col } from '@uprise/grid';
import moment from "moment-timezone";

import Calendar from '../../../core/components/Calendar';
import Dropdown from '../../../core/components/Dropdown';

import Header from '../../../core/components/Header';
import Card from '../../../core/components/Card';
import Button from '../../../core/components/Button';
import { DropdownData } from './types';
import { timeZones, callDurations } from '../utils/dates';

interface Props {
    fetchFreeSlots: (date: Object | null, timezone: string, slotDuration: number) => Promise<void>;
}

interface State {
	date: moment.Moment | null;
	timezone: string;
    slotDuration: number;
    focused: boolean | null;
}

export default function SlotSettings(props: Props) {
	const [date, setDate] = useState<State["date"]>(null);
    const [timezone, setTimezone] = useState<State["timezone"]>(moment.tz.guess());
    const [slotDuration, setSlotDuration] = useState<State["slotDuration"]>(30);

    const onDateChange = (date: moment.Moment | null) => {
		if(date) {  
			setDate(date);
		}
	}
	const onTimezoneChange = (timezone: DropdownData) => {
		setTimezone(timezone.value as string);
	}

	const onSlotDurationChange = (slotDuration: DropdownData) => {
		setSlotDuration(slotDuration.value as number);
	}

    const handleClick = (e: any) => {
        props.fetchFreeSlots(date, timezone, slotDuration)
    }

    const onFocusChange = ( arg: any  ) => {
        arg.focused = true;
    };

    return (
        <>
            <Header title="Book a call" />
            
            <Container>
                <Card>
                    <Row height="400px">
                        <Col className="col-lg-6">
                            <Calendar
                                title="Select a Date"
                                onDateChange={onDateChange}
                                date={date}
                                focused={true}
                                blockPastDates={true}
                                onFocusChange={onFocusChange}
                            />
                        </Col>
                        <Col className="col-lg-6">
                            <Dropdown
                                name="timezone"
                                title="Select your timezone"
                                options={timeZones}
                                handleInputChange={onTimezoneChange}

                            />

                            <Dropdown
                                name="slotDuration"
                                title="Select call duration"
                                options={callDurations}
                                handleInputChange={onSlotDurationChange}
                            />
                        </Col>
                    </Row>
                    <Row height="100px">
                        <Col>
                            <Button
                                title="Get Free Slots"
                                handleClick={handleClick}
                            />
                        </Col>
                    </Row>
                </Card>
            </Container>
        </>
    )
}
