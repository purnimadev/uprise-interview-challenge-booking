export interface DropdownData {
    label: string;
    value: string | number;
}