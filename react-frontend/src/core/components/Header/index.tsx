import React from 'react';
import styled from 'styled-components';
// @ts-ignore
import { ContainerFluid, Row, Col } from '@uprise/grid';
// @ts-ignore
import { H2, H3 } from '@uprise/headings';
// @ts-ignore
import { backgrounds } from '@uprise/colors';
// @ts-ignore
import { ProgressiveImage } from '@uprise/image';
import logo from '@uprise/icons/icons/png/logo-head/logo-head.png';


interface Props  {
    title: string;
}

export default function Header(props: Props) {
    const { title } = props;
    return (
        <StyledHeader>
            <ContainerFluid>
                <Row>
                    <Col className="col-lg-4">
                        <ProgressiveImage src={logo} overlaySrc=''/>
                        {/* <H3>
                            uprise
                        </H3> */}
                    </Col>
                    <Col className="col-lg-4">
                        <StyledTitle>
                            {title}
                        </StyledTitle>
                    </Col>
                    <Col className="col-lg-4">
                    
                    </Col>
                </Row>
            </ContainerFluid>
        </StyledHeader>
            
    )
}

const StyledHeader = styled.div`
    background-color: ${backgrounds.white};
    margin-bottom: 50px;
    padding: 30px;
    cursor: pointer;
`;

const StyledTitle = styled(H2)`
    margin-top: 0px;
    margin-bottom: 0px
`;