import React from 'react';
import styled from 'styled-components';

//@ts-ignore
import { Button as UpriseButton } from '@uprise/button';
//@ts-ignore
import { secondary } from '@uprise/colors';

interface Props {
    handleClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
    title: string;
    value?: string;
    size?: string;
    fullWidth?: boolean;
    variant?: "primary" | "secondary" | "tertiary";
}

export default function Button({variant="primary", ...props}: Props) {
    return (
        <StyledUpriseButton 
            title={props.title} 
            size={props.size} 
            variant={variant}
            onClick={props.handleClick}
            value={props.value}
        />
    )
}

const StyledUpriseButton = styled(UpriseButton)`
    background-color: ${props => (props.variant === "primary" ? secondary.electricPurple : null)};
`;