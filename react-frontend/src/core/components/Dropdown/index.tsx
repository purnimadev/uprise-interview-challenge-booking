import React from 'react';
import Select, { Styles } from 'react-select';
// @ts-ignore
import { H5 } from '@uprise/headings';
// @ts-ignore
import { backgrounds, primary, secondary } from '@uprise/colors';

interface Props {
    name: string;
    title: string;
    options: any[];
    handleInputChange: (value: any) => void;
}

const customStyles: Partial<Styles> = {
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
        return {
            ...styles,
            color: isFocused ? secondary.electricPurple : primary.charcoal,
            backgroundColor: isFocused ? backgrounds.fadedPurple : backgrounds.white
        } 
    },  
}



export default function Dropdown(props: Props) {
    const {title, options, handleInputChange, name } = props;

    return (
        <>
            <H5 textAlign="left">
                {title}
            </H5>

            <Select
                options={options}
                styles={customStyles}
                onChange={handleInputChange}
            />
        </>
    )
}
