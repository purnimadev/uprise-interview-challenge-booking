import React, { ReactNode } from 'react';

// @ts-ignore
import { Card as UpriseCard } from '@uprise/card';
// @ts-ignore
import { backgrounds } from '@uprise/colors';

interface Props {
    className?: string;
    padding?: string;
    width?: string;
    border?: string;
    shadow?: boolean;
    backgroundColor?: string;
    children?: ReactNode;
} 

export default function Card({
    backgroundColor=backgrounds.white,
    padding='50px',
    ...props}: Props) {
    return (
        <UpriseCard
            backgroundColor={backgroundColor}
            padding={padding}
            {...props}
        >
            {props.children}
        </UpriseCard>
    )
}
