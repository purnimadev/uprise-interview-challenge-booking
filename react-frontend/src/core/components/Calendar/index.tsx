import React from 'react';
import styled from 'styled-components';

import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import {
    DayPickerSingleDateController,
    DayPickerSingleDateControllerShape,
    isInclusivelyAfterDay,
} from "react-dates";

import moment from 'moment';

// @ts-ignore
import { H5 } from '@uprise/headings';
// @ts-ignore
import { secondary } from '@uprise/colors';

interface Props extends DayPickerSingleDateControllerShape {
    title: string;
    blockPastDates?: boolean;
    onDateChange: (date: moment.Moment | null) => void
}


const isOutsideRange = (day: moment.Moment) => !isInclusivelyAfterDay(day, moment())

function Calendar(props: Props) {
    const {
        title,
        blockPastDates,
        ...datePickerProps} = props;

    return (
        <>
            <H5 textAlign="left">
                {title}
            </H5>
                <StyledDatePicker>
                <DayPickerSingleDateController
                    {...datePickerProps}
                    hideKeyboardShortcutsPanel={true}
                    isOutsideRange = {blockPastDates ? isOutsideRange : undefined} 
                  
                />
                </StyledDatePicker>
                
            
        </>
    )
}

export default Calendar;

const StyledDatePicker = styled.div`
    .CalendarDay__selected {
        background: ${secondary.electricPurple};
        color: white;
    }
`;