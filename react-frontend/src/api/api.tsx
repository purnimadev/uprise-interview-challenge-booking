
import axios from 'axios';
const apiUrl = 'http://localhost:8000/api/v1';

const getHeaders = () => {
    return {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    };
};

export const api = {
    get: (date: Object | null, timezone: string, slotDuration: number) => {
        return new Promise((resolve, reject) => {
            axios.get(`${apiUrl}/doctors/YdQKdOxB0L2nAi2WvHim/slots/?date=${date}&&timezone=${timezone}&&slotduration=${slotDuration}`, getHeaders())
                .then(response => { resolve(response) })
                .catch(error => { reject(new Error(error)) });
        });
    },
    post: (date: Object | null, slotDuration: number) => {
        return new Promise((resolve, reject) => {
            axios.post(`${apiUrl}/events`,{ date, slotduration: slotDuration }, getHeaders())
                .then(response => { resolve(response) })
                .catch(error => { reject(new Error(error)) });
        });
    }
}
