This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Steps to run the react app
- Clone the repo
- Install node_modules using `npm install`
- Open [http://localhost:3000](http://localhost:3000) to view it in the browser

## Steps to run the express app
- Clone the repo
- Install node_modules using `npm install`
- Open [http://localhost:8000](http://localhost:8000) to view it in the browser
- If you get error when starting the server, please check if you set correct path to your firebase keys.

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
